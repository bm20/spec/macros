#%TITLE% SAVEOPTICS.MAC
#%NAME%
#Macros to save and restore the configurations of the ROBL optics
#%DESCRIPTION%
#A macro set to save and restore the current motor positions of the
#beam line optics. The motor positions are stored in a plain ASCII file
#for retrieval and possible later data evaluation. Please refer to the
#dedicated document for detailed documentation of those macros.
#%END%

#%DEPENDENCIES%
# mirrctrl.mac
# monoctrl.mac
#%END%

#%INTERNALS% 
#The positions are saved to and retrieved from the file %BR%
#/users/blissadm/local/spec/userconf/optics/optics_settings
#%END%

#%EXAMPLE%
#%DL%
#%DT%save_rch_optics 20-01-725 Christoph Hennig
#%DD%Saves the current motor positions of the optics session with the comment "20-01-725 Christoph Hennig".
#The comment can be searched for.
#%DT%restore_optics
#%DD%Prompts the user for the desired optics configuration and search string. If several savings are 
#found, the user will be presented with a list to choose from.
#%DT%restore_optics 1323 
#%DD%Searches for the optics configuration 1323, i.e. for Pt mirrors ("3") and Si(111)/0deg ("2") for
#the RCH ("1"). If several entries are found, the user is prompted to choose from a list.
#%DT%restore_optics hennig
#%DD%Searches the database for the search string "hennig" (case insensitive) and prompts the user
#for the desired configuration. The search string can be changed as well. If several e ntries are
#found, the user is prompted by a list to choose from.
#%DT%restore_optics 1323 hennig
#%DD%Searches the database for the configuration 1323 AND "hennig" (case insensitive). 
#If several entries are found, the user is prompted to choose from a list.
#%DT%restore_optics 0000 hennig
#%DD%Searches case insensitive for "hennig" and does not prompt the user for a particular configuration
#%XDL%
#%END%



need mirrctrl.mac
need monoctrl.mac

#constant optics_file "/users/blissadm/local/spec/spec.d/optics/optics_settings" 
#constant optics_file "/users/blissadm/local/userconf/optics/optics_settings" 
constant optics_file "/users/blissadm/local/spec/userconf/optics/optics_settings" 

#%IU%(hutch_number)
#%MDESC%
#Converts the hutch number to a human readable string: 1:"RC", 2:"MR", or "unknown"
def _robl_hutch(hutch) '{
    if (hutch==1) return ("RC")
    if (hutch==2) return ("MR")
    return("unknown")
}'

#%IU%(hutch_number)
#%MDESC%
#returns the optics configuration code of the current motor positions and adds the submitted hutch number.
def _build_optics_config(hutch) '{

    local confstr

     confstr = "0000"
    _check0 m1lat
    _check0 monxtal
    _check0 m2lat

    if ((hutch < 1) && (hutch > 2)) 
        eprintf("Invalid hutch number:  %i\n",hutch) 
    confstr = sprintf("%i%i%i%i",hutch, m1_surface(),A[monxtal], m2_surface())

    return (confstr)
}'

#%IU%(hutch_number)
#%MDESC%
#Parses the %B%optics_settings%B% file for the (last) header line of the motor
#positions, starting with "#@conf". Returns the entire line.
def _read_motorlist() '{
  local motorstr _i
  motorstr = ""

  ret = getline(optics_file,"open")

  if (ret==0) { #If opened sucessfully
     while (ret != -1) {
        ret = getline(optics_file)
        if (substr(ret,1,6)=="#@conf")
           motorstr = ret
     }
     getline(optics_file,"close")
  }
  
  if (substr(motorstr,1,6) == "#@conf") {
      # Strip trailing comments as well as \r and \n
      if ((_i=index(substr(motorstr,2),"#")) > 0) motorstr = substr(motorstr,1,_i)
      if ((_i=index(motorstr,"\r")) > 0) motorstr = substr(motorstr,1,_i-1)
      if ((_i=index(motorstr,"\n")) > 0) motorstr = substr(motorstr,1,_i-1)

      return(motorstr)
  } else {
      eprintf("Error: No valid headline found in optics_settings.")
      exit
  }

}'

############################################################
## SAVE PART
############################################################

#%IU%(hutch_number, comment)
#%MDESC%
#The core macro for the saving: Searches the last header
#line in %B%optics_settings%B% via %B%_read_motorlist()%B% and saves
#the positions of those motors found in that header line.
def _save_optics(hutch,comment) '{

  local motorstr
  local motorlist, nmot
  local positionsstr

  motorstr = _read_motorlist()
  nmot = split(motorstr,motorlist) 

  positionsstr = _build_optics_config(hutch)

  if (positionsstr == "") {
    eprintf("Error: No valid headline found in optics_settings.")
    exit
  }
  
  for (i=1;i<nmot;i++) {
    newpos = "void"

    #strip trailing component assignments: We save it anyway
    if (index(motorlist[i],":") > 0) {
        motorlist[i] = substr(motorlist[i],1,index(motorlist[i],":")-1)
    }

    if (substr(motorlist[i],1,1) == "@") { #strip leading "@" 
       motorlist[i] = substr(motorlist[i],2)
    }

    if (substr(motorlist[i],1,1) == "&") { #strip leading "&" 
       motorlist[i] = substr(motorlist[i],2)
    }

    if (motorlist[i] == "epoch") {
         newpos = sprintf("%i",int(time()))
    } 
    if (motorlist[i] == motor_mne(motor_num(motorlist[i]))) {
      # if valid motor mnemonic
         newpos = sprintf("%g",A[motor_num(motorlist[i])])
    } 
    if (motorlist[i] == cnt_mne(cnt_num(motorlist[i]))) {
      # if valid counter mnemonic
         newpos = sprintf("%g",S[cnt_num(motorlist[i])])
    }  

    if (newpos != "void") 
        positionsstr = sprintf("%s %s",positionsstr,newpos)
    else {
        eprintf("Invalid mnemonic in optics_settings: \"%s\".\n", motorlist[i])
        exit
    }
  }


  # Add date and configuration in human readable form as comment
  positionsstr = sprintf("%s \# \"%s; Configured for %s hutch: M1:%s, DCM:%s, M2:%s",\
      positionsstr, date(), _robl_hutch(hutch),\
      surface_name(m1_surface()), \
      monocrystalname(),\
      surface_name(m2_surface()))

  if (comment != "")
       positionsstr = sprintf("%s; %s", positionsstr, comment)
 
  positionsstr = positionsstr "\"\n"

  # make a backup copy of the old optics_settings file
  cmdstr=sprintf("cp -f %s %s.bak",optics_file,optics_file)
  unix(cmdstr)

  # and write the new position string to the optics_settings file
  fprintf(optics_file,"%s",positionsstr)
  close(optics_file)

}'

#%UU% [comment]
#%MDESC%
#Saves the positions of the optics motors for the RCH.
def save_rch_optics '{
  _save_optics(1,"$*")
}'

#%UU% [comment]
#%MDESC%
#Saves the positions of the optics motors for the MRH.
def save_mrh_optics '{
  _save_optics(2,"$*")
}'

############################################################
## RESTORE PART
############################################################

#%IU%(candidate, search_code)
#%MDESC%
#Check whether a read configuration (candidate) matches
#the search pattern (search_code) and returns "1" if 
#the candidate matches the search; otherwise 0.
#
#In search_code, "0" acts as a wildcard which matches any config.
def _conf_found(cand,conf) '{
  local i s c

  if ((length(cand) != 4) || (length(conf) != 4))
     return(0) # Error, just ignore it

  for(i=1; i<=4; i++) {
     s = substr(cand,i,1)
     c = substr(conf,i,1)
     if ((c != "0") && (s != c)) return(0)
  }

  return(1) # If  vaild pattern found
}'

#%IU%(poslist, ctr)
#%MDESC%
#Outputs the list of found configurations, prompts the user for 
#the one he wants to restore, and returns the selected line for
#further processing.
def _list_saved_versions(poslist,posctr) '{
  local i, key 
  local conf, d
  local _aux
  local _c, _i, c

  print
  print "Found " posctr " entries, which matches your query:"
  print

  for(key=1; key<=posctr; key++) {

     # Extract configuration and time
     split(poslist[key],_aux)
     conf = _aux[0]
     d = date(int(_aux[1]))
     c = "" # Clear comment field (for in case, no comment is given)

     #Extract comment from line
     _i = split(poslist[key], _c, ";")
     # _c[2] : Text after second occurance of ";" --> comment
     #  strip trailling chars (newline and doublequote
     sscanf(_c[2],"%[^\"\r\n]",c) 

    printf(" %3i : %s (%s:%2.0fkeV:%s|%s|%s) :%s\n", key, d, \
             _robl_hutch(substr(conf,1,1)),\
             0.123,\
            surface_name(substr(conf,2,1)),\
            crystal_name(substr(conf,3,1)),\
            surface_name(substr(conf,4,1)), c)
  }
  
  key = getval("\nWhich configuration do you want to restore",posctr)

  if ((key < 1) || (key > posctr)) {
     print "\nOkay, got it. Aborting."
     exit
  } else {
     return (poslist[key])
  }

}'

#%IU%
#%MDESC%
#Parses the %B%optics_settings%B% file and returns the found (and user selected) configuration.
def _read_optics_positions(conf, searchstr) '{
  local motorstr
  local _aux
  local poslist[]
  local posctr
  motorstr = ""
  posctr = 0

  ret = getline(optics_file,"open")

  if (ret==0) { #If opened sucessfully
     while (ret != -1) {
        ret = getline(optics_file)
        split(ret,_aux)
        if ((_conf_found(_aux[0],conf)) && \
            ((length(searchstr) == 0) || (index(tolower(ret),searchstr) > 0))) {
           # p "Config found: ", ret
           # motorstr = ret
           posctr++
           poslist[posctr] = ret 
        }
     }
     getline(optics_file,"close")
  }
  
 if (posctr == 0) {
        printf("Sorry, I couldn\'t find the requested configuration: %s.\n",conf)
        exit 
 } else {
        motorstr = _list_saved_versions(poslist,posctr)
        # Strip comment
        motorstr = substr(motorstr,1,index(motorstr,"#")-1)
        return(motorstr) 
 }
}'


#%IU%
#%MDESC%
#Outputs a list of the current positions of the motor to be moved and 
#their target positions.
#
# Stolen from Gerry. Was: show_motor_info
def _print_conf(mne,pos,npos) '{

    local i, j, k, m, s[], t, f, u[], n, c
    local sfmt, ffmt, cols, p, w
    local aux

    f[0] = "name"
    f[1] = "mne"
    f[2] = "user"
    f[3] = "newpos"
    ns = 4 # Number of lines, i.e. lines in f[]
    n  = npos # Number of postions in pos list

    c = 0 
    for (k=0; k < npos; k++){
       if (mne[k] != "") c++ 
    }

    p = _mprec(mA, n)   # p is precision
    w = p + 5           # w is width
    tty_cntl("resized?")
    cols = int(COLS / (w + 1))
    if (MOTOR_FMT_COLS > 0 && MOTOR_FMT_COLS < cols)
        cols = MOTOR_FMT_COLS
    sfmt = sprintf("%%%d.%ds", w, w)
    ffmt = sprintf("%%%d.%df", w, p)

    u[0] = sprintf(sfmt,"")
    u[1] = sprintf(sfmt,"")
    u[2] = sprintf(sfmt,"Current Pos")
    u[3] = sprintf(sfmt,"Saved Pos")
     
    # distribute evenly over needed rows
    if (c > (cols - 1)) {
        i = int(c / (cols - 1)) + 1  # Number of needed rows
        cols = int(c / i) + 1  # Cosmetic distribution, alternatively, could be "cols = cols - 1"
    }

    for (i = 0; i < n && i < npos; i++) {
        for (k = 0; k < ns; k++)
            s[k] = ""
        for (j = 0; i < n && i < npos; i++) {
            if (mne[i]=="")
                continue;
            for (k = 0; k < ns; k++) {
                aux = mne[i]
                if (substr(aux,1,1) == "&") {
                    # Strip leading marker
                    aux = substr(aux,2)
                } 
                m = motor_num(aux)
                if (f[k] =="name")
                    s[k] = s[k] sprintf(sfmt, motor_name(m))
                else if (f[k] == "mne")
                    s[k] = s[k] sprintf(sfmt, motor_mne(m))
                else if (f[k] == "user")
                    s[k] = s[k] _mfit(w, p, sprintf(ffmt, A[m]))
                else if (f[k] == "newpos") {
                        if (pos[i]=="---") 
                               s[k] = s[k] sprintf(sfmt, "---")
                        else
                               s[k] = s[k] _mfit(w, p, sprintf(ffmt, pos[i]))
                     }
            }
            if (j%cols == (cols-1))
                break
            for (k = 0; k < ns; k++)
                s[k] = s[k] " "
            j++
        }
        for (k = 0; k < ns; k++)
            print u[k] s[k]
        # newline between motor rows ...
        if (i < n && !MOTOR_FMT_COMPACT)
            print
    }


 # for(i=0; i<npos; i++) {
 #   if (mne[i] != "") {
 #       printf("%10s %7s %g  %s\n",motor_name(motor_num(mne[i])), mne[i], A[motor_num(mne[i])], pos[i])
 #   }
 # }

}'


#%IU%(column_header, components_list) 
#%MDESC%
#Takes the coulmn header for this motor
#and checks whether
#it belongs to a group of motor (mirror 1 / DCM / mirror2) which has been
#specified in components_list (1=M1, 2=DCM, 4=M2) and
#thus shall be restored.
#
#Returns 1 (true) or 0 (false)
def _restore_this_component(compname, complist) '{

  if (complist == 7) return(1) # Restore all axes

  if ((complist & 1) && (index(compname,"m1" )>0)) return(1)
  if ((complist & 2) && (index(compname,"dcm")>0)) return(1)
  if ((complist & 4) && (index(compname,"m2" )>0)) return(1)

  return(0)
}'


#%IU%
#%MDESC%
#The core macro for restoring the motor postions.%BR%
#
#It parses the %B%optics_settings%B% file, filters them for the
#search conditions, prompts the user to choose from the list,
#presents a preview of the movements, and starts the move.
def _restore_optics(search_conf, searchstr) '{
  local motorstr mne nmot
  local posistr pos npos
  local i d comp answ grp 
  local conf 
  local motlist
  local aux
  local complist

  # Read header line with mnemnonics
  motorstr = _read_motorlist()
  nmot = split(motorstr,mne) 

  # scan the table of saved positions, ask user which one s/he wants and
  # returns positions in posistr
  posistr = _read_optics_positions(search_conf, searchstr) 
  npos = split(posistr,pos)
  conf = pos[0]  # The found configuration, which matches search_conf

  complist = _query_complist() # Asks user, which components to restore

  # Clean up things: Keep only motor mnemonics, which we want to move:

  for(i=0; i<npos; i++) {
    # print i, mne[i], pos[i]

    if (mne[i]=="@epoch") {
         # save the saving date
         d = date(int(pos[i]))
    } 

    if (substr(mne[i],1,1) =="@") {
         #Delete all mnemonics which start with @
         mne[i] = ""
    }

    
    grp  = index(mne[i],":") 
    comp = ""
    if (grp > 0) {
       # Strip the component assignment
       comp   = substr(mne[i],grp) 
       mne[i] = substr(mne[i],1,grp-1)
    }
    if (!_restore_this_component(comp,complist)) { 
       mne[i] = ""
    }
           
    # Remove leading marker for next check
    aux = mne[i]
    if (substr(aux,1,1) == "&") {
        aux = substr(aux,2) 
    }
    if (motor_mne(motor_num(aux)) != aux) {
         #Delete all invalid motor mnemonics
         mne[i] = ""
    }
    if (sprintf("%s",pos[i]) != sprintf("%g",pos[i])) {
       # mark everything where position appears to be not a number
       pos[i] = "---"
    }
  }

  print

  printf("You are about to restore a beamline optics configuration:\n\n")
  printf("Found a saved configuration for the %s hutch\n\n   Mirror 1: %s, Mono: %s, Mirror 2: %s \n\nwhich was saved on %s:\n\n",\
             _robl_hutch(substr(conf,1,1)),\
            surface_name(substr(conf,2,1)),\
            crystal_name(substr(conf,3,1)),\
            surface_name(substr(conf,4,1)),d)

  waitmove
  getangles

  _print_conf(mne,pos,npos)

  printf("\nPlease check, whether this is what you want.\n")
  answ = yesno("\nDo you want to proceed",0)

  motlist = ""
  if (answ == 1) {
      # First round: Move motors, which are NOT marked with leading "&" 
      for(i=0; i<npos; i++) {
        if ((mne[i] != "") && (pos[i] != "---") && (substr(mne[i],1,1) != "&")) {
            A[motor_num(mne[i])] = pos[i]
            # motlist = motlist " " sprintf("%i",motor_num(mne[i]))
             motlist = motlist " " motor_num(mne[i])
        }
      }
      move_em

      print "Here we go... ";

      sleep(0.3) # Give Icepap a chance...
      _update(motlist)

      # Second round: Move motors, which are marked with leading "&" for after-move
      motlist = ""
      for(i=0; i<npos; i++) {
        if ((mne[i] != "") && (pos[i] != "---") && (substr(mne[i],1,1) == "&")) {
            aux = substr(mne[i],2)
            A[motor_num(aux)] = pos[i]
            # motlist = motlist " " sprintf("%i",motor_num(mne[i]))
             motlist = motlist " " motor_num(aux)
        }
      }
      move_em

      print "...moving after-move motors...";

      sleep(0.3) # Give Icepap a chance...
      _update(motlist)

  }
}'

#%IU%
#%MDESC%
#Checks in detail whether the submitted configuration code is 
#valid or not. 
def _check_confstr(conf) '{

   local _h

   if (length(conf) != 4) {
      eprintf("Wrong argument length. Must be four (4) digits.")
   } 

   if (index("012",(_h=substr(conf,1,1))) == 0) {
      eprintf("Wrong hutch number %s. Must be 0, 1: RCH or 2: MRH.\n",_h)
      exit
   } 
   if (index("0123",(_h=substr(conf,2,1))) == 0) {
      eprintf("Wrong surface number %s for mirror 1.\n",_h)
      eprintf("Must be 0, 1: %s, 2: %s, or 3: %s.\n",\
            MIRRNAME[1],MIRRNAME[2],MIRRNAME[3])
      exit
   } 
   if (index("012345",(_h=substr(conf,3,1))) == 0) {
      eprintf("Wrong surface number %s for mono.\n",_h)
      eprintf("Must be 0, 1: %s, 2: %s, 3: %s, 4: %s, or 5: %s.\n",\
           DCMXTALNAME[1], DCMXTALNAME[2], DCMXTALNAME[3], DCMXTALNAME[4],DCMXTALNAME[5])
      exit
   } 
   if (index("0123",(_h=substr(conf,4,1))) == 0) {
      eprintf("Wrong surface number %s for mirror 2.\n",_h)
      eprintf("Must be 0, 1: %s, 2: %s, or 3: %s\n",\
            MIRRNAME[1],MIRRNAME[2],MIRRNAME[3])
      exit
   } 
}'

#%IU%
#%MDESC%
# Prompts the user to enter the settings of the different components.
# Returns the (unchecked) configuration code.
def _query_conf() '{
      local s hu m1 m2 mo

      s = sprintf("Enter Hutch Number:\n")
      s = sprintf("%s   0: ignore, 1: RCH, or 2: MRH.",s)
      hu = getval(s,hu)

      s = sprintf("Enter surface of mirror 1:\n")
      s = sprintf("%s   0: ignore, 1: %s, 2: %s, or 3: %s.",s, \
            MIRRNAME[1],MIRRNAME[2],MIRRNAME[3])
      m1 = getval(s,m1)

      s = sprintf("Enter crystal of monochromator:\n")
      s = sprintf("%s   0: ignore,\n   1: %s,\n   2: %s,\n   3: %s,\n   4: %s, or\n   5: %s.\nWhich crystal", s,\
           DCMXTALNAME[1], DCMXTALNAME[2], DCMXTALNAME[3], DCMXTALNAME[4],DCMXTALNAME[5])
      mo = getval(s,mo)

      s = sprintf("Enter surface of mirror 2:\n")
      s = sprintf("%s   0: ignore, 1: %s, 2: %s, or 3: %s.",s, \
            MIRRNAME[1],MIRRNAME[2],MIRRNAME[3])
      m2 = getval(s,m2)

     s = sprintf("%s%s%s%s",hu,m1,mo,m2)

     return(s)
}'


#%IU%
#%MDESC%
# Prompts the user to enter which components to restore.
# Returns the (unchecked) components code.
def _query_complist() '{
      local s, answ

      answ = 7
      s = sprintf("\nWhich components do you want to restore:\n")
      s = sprintf("%s   1: 1st Mirror, only\n   2: Monochromator, only\n   3: 1st Mirror + Monochromator\n   4: 2nd Mirror, only\n   5: 1st Mirror + 2nd Mirror\n   6: 2nd Mirror + Monochromator\n   7: Everything, incl. slits etc.\nWhich components",s)

      answ = getval(s,answ)

      if ((answ < 1) || (answ > 7)) {
          printf("Invalid answer: %s\n",answ)
          exit
      }

      return(answ)
}' 

#%UU% [config_code] [search_string]
#%MDESC%
#Restores the motor positions previously saved by %B%save_xxx_optics%B%.
#
#If invoked without arguments, the user is prompted (guided) for the input.
#
#The config_code is a four-digit string, where the first to fourth digit stand
#for the experiment hutch (1:RCH, 2:MRH), the first mirror (1:Rh, 2:Si, 3:Pt),
#the DCM/DMM crystal/ML (1:Si(311), 2:Si(111)/0deg, 3:Si(111)/30deg, 4:ML(25A), 5:ML(37A)),
#and the second mirror (cf. M1), respectively.
#
#A zero (0) on at a certain position acts as a placeholder. Its value is ignored in the
#search and the motors belonging to this component are NOT restored. 
#
#If all digits are non-zero, all motors (even those which do not belong to M1, DCM, or M2)
#are restored (e.g. slits)
#
#If %B%search_string%B% is given, the search is (furthermore) restricted to this string
#(case insensitive).
#
#Refer to section EXAMPLE to see examples on the usage.
def restore_optics '{
  local confstr
  local searchstr
  local c1,c2,c3,c4


   searchstr = tolower("$*") # By default take everything given as search string
   confstr = ""

  # If first argument is a valid configuration string take it
  # otherwise interpret everything as search string
  if ($# > 0) {

      # Extract first argument and 
      # Quick-and-dirty check whether it is formally valid:
      # If all digits are in range, sscanf has to return a four digit long string
          c1=c2=c3=c4=""
          sscanf("$1","%1[0-2]%1[0-3]%1[0-4]%1[0-3]",c1,c2,c3,c4);      
          confstr = c1 c2 c3 c4

          if (length(confstr) == 4) {
             # confstr vaild, strip confstr from searchstr rest of line
             # searchstr = substr(searchstr,index(searchstr," ")) 
             searchstr = substr(searchstr,5) 
             while (substr(searchstr,1,1) == " ") searchstr = substr(searchstr,2)
          } else {
             # confstr invalid, leave searchstr untouched
             confstr = ""
          }
   }

   if (confstr == "") {
       confstr = _query_conf()
       searchstr = getval("\nFor which string do you want to search", searchstr)
   }

  _check_confstr(confstr)
  _restore_optics(confstr, searchstr)
}'


#%MACROS%
#%IMACROS%
#%AUTHOR% 
# Author : Randolf Butzbach
# Date   : Thu Aug 15 13:32:16 CEST 2013 : Initial version
# Date   : Fri Mar 21 10:29:24 CET 2014 : Added list of saved configurations and searching for a string
# Date   : 09/12/2014 : Added two-steps movements
# Date   : 20/01/2015 : Added components selection prompt.
#%TOC%

