#%TITLE% APS_style_collect.MAC
#%NAME%
# Macros in order to take data "APS Style" at ESRF %BR%
# Based on input from: 
#%BR% Peter Eng, Joanne Stubbs (APS) and Dr Moritz Schmidt (HZDR)
#%DESCRIPTION%
# This macro set provides macros for overwriting some commands. %BR%
# Others will need to have a different name (sorry)
#%END%

#%UU% rootdir
#%MDESC%
# This will set the root filename to be used with all newccdfile %BR%
# This in order to convert newccdfile with sample and scan to ccdnewfile at ESRF %BR%
# Example setrootdir /data/bm20/inhouse/EH2/2017/ctr5
def setrootdir '{
  global APS_ROOTDIR
  if ( $# != 1 ) { 
    print "Usage: setrootdir <rootdir>"
    print "   setrootdir /data/bm20/inhouse/EH2/2017/ctr6"
    return (1)
  }
  APS_ROOTDIR = "$1"
}'


#%UU% <sample name> <scan number> 
#%MDESC%
#  This will convert APS style to ESRF style (ccdnewfile)
def newccdfile '{
  local rootdir sample_name scan_marker mydir returnCode
  local scan_number file_dir command str_num
  local old_scan_number old_sample_name
  # first check that rootdir is set 
  if ( APS_ROOTDIR == 0 ) { 
    print "APS_ROOTDIR not set, please run setrootdir first"
    return (1)
  } 
  # $1 - sample name
  # $2 - scan number
  if ( $# > 2) { 
     print "Usage: newccdfile <sample name> <scan number>" 
     print "  The \"scan marker\" be S<3 digit number> like S012 for <scan number> being 12"
     print "This will run ccdnewfile 1 4 \\"
     print "  \"<rootdir>/images/ \\"
     print "    /<sample name>/scan marker>\" \"<sample name>_<scan marker>_\" .tiff 0"
     return (1)
  }
  #
  # Check first current sample_name and sample_number
  # 
  # resetup to update the spec cache
  ccdresetup CCD_U 
  # get last scan_number and last sample_name
  file_dir = image_par(CCD_U,"file_dir")
  # sample we get from the last written file with LIMA (From DS!)
  old_sample_name = basename(dirname(file_dir))
  # The last scan we get from the scan part of SPEC
  old_scan_number = SCAN_N
  if ( $# == 2 ) { 
     sample_name = "$1" 
     scan_number = "$2"
  } 
  # Check if only sample_name given 
  if ( $# == 1 ) { 
     sample_name = "$1"
     if ( old_sample_name == sample_name ) { 
        scan_number = old_scan_number+1
        if (scan_number > 999 ) { 
           print "newccdfile error: next scan number will be above 999"
           print "Please change sample name"
           return(1)
        } 
     } else {
        scan_number = "0"
     }
  } 
  # Check if nothing is given
  if ( $# == 0 ) { 
    sample_name = old_sample_name
    scan_number = old_scan_number+1
    if (scan_number > 999 ) { 
        print "newccdfile error: next scan number will be above 999"
        print "Please change sample name"
        return(1)
    }
  }

  # APS_ROOTDIR is already set
  rootdir = APS_ROOTDIR
  if (file_info(sprintf("%s/images",rootdir), "isdir") == 0) {
     unix(sprintf("mkdir -p %s/images", rootdir))
     if (file_info(sprintf("%s/images",rootdir), "isdir") == 0) {
        printf("newccdfile error: Failed to create directory %s\n",rootdir) 
        return (1)
     } 
   }
   # create scan_marker from scan_number
   scan_marker=sprintf("S%03d",scan_number)
   # subdirectory
   mydir=sprintf("%s/images/%s/%s",rootdir,sample_name,scan_marker)
   unix(sprintf("mkdir -p %s", mydir))
   if (file_info(mydir, "isdir") == 0) {
       printf("newccdfile error: Failed to create directory %s\n", mydir)
       return (1)
   } 
   command = sprintf("ccdnewfile 1 4 \"%s\" \"%s_%s_\" .tiff 0",mydir,sample_name,scan_marker)
   print command
   eval(command)
}'


#%UU% energy
#%MDESC% 
# moves monochromator to Energy %BR%
# and calculates wavelength for the orientation matrix%BR%
def erglam '{
if ($# != 1) {
        eprint "Usage:  erglam   energy"
        exit
    } 
    _E = $1
    
    mv energy _E
    LAMBDA = 12398.416/_E
}'


#%UU% motor start finish  time <header flag = 0 or 1>
#%MDESC% 
# example of usage %BR%
# LocalEn = Eo-10 %BR%
# erglam LocalEn %BR%
# ubr H K L  %BR%
# raxr_ascan energy LocalEn LocalEn t %BR%
# This is coming from ascan %BR%
# But does only one datapoint - 5 arguments only (without intervals but adding a 0/1 for headers) %BR%
def raxr_ascan '{
        if ($# != 5) {
                eprint "Usage:  raxr_ascan  motor start finish  time <header flag = 0 or 1>"
                exit
        } 
        _check0 "$1"
        _header_for_raxr_ascan = $5
        # : separated list of arguments for _raxr_scan_prep
        args = sprintf("ascan \"%s:%d:%d:0:%d\"",$1,$2,$3,$4)
        eval(sprintf("_raxr_scan_prep %s",args))
}'



#%UI% 
#%MDESC% 
# this is _angle_scan_prep from 6.06.11 with ":" instead of "\a" as $2 separator
# and _nm = 1 fixed and HEADING = "raxr_ascan" independent of scan
def _raxr_scan_prep '{
    local _i, _j, _ac, _av[], _is_dscan, _scan, _num

    _scan = "$1"

    _ac = split("$2", _av, ":")

    if (substr(_scan, 1, 1) == "d")
        _is_dscan = 1

    if (sscanf(_scan, "%*[ad]%dscan", _i) != 1)
        _i = 1

    if (_ac != _i * 3 + 2) {
        local   s

        s = "Usage: " _scan " "
        if (_i > 1)
            s = s "("
        if (_is_dscan)
            s = s "mot relative_start relative_end"
        else
            s = s "mot start end"
        if (_i > 1)
            s = s ")*" _i
        s = s " intervals time"
        eprint s
        exit
    }
    _ctime = eval2(_av[--_ac])
    _m1 = 0
    _num = eval2(_av[--_ac])
    # OLD _nm = _ac / 3
    _nm = 1

    if (_ac%3) {
        eprintf("%s: Wrong number of arguments.\n", _scan)
        exit
    }

    _make_SCAN_POINTS(_scan, _num + 1, _nm)

    if (_is_dscan) {
        _scan = sub("d", "a", _scan)
        waitall
        getangles
    }
    # OLD HEADING = sprintf("%s ", _scan)
    HEADING = "raxr_ascan"

    for (_i = 0; _i < _ac; _i += 3) {
        local _cen, _begin, _delta, _end

        if ((_m[_i/3] = motor_num(_av[_i])) < 0) {
            eprintf("$1: Invalid motor: \"%s\"\n", _av[_i])
            exit
        }
        _begin = eval2(_av[_i+1])
        _end = eval2(_av[_i+2])
        if (_num)
            _delta = (_end - _begin) / _num
        for (_j = 0; _j <= _num; _j++)
            SCAN_POINTS[_j][_i/3] = _begin + _j * _delta

        _cen = _is_dscan? A[_m[_i/3]]:0
        HEADING = HEADING sprintf(" %s %g %g ", motor_mne(_m[_i/3]), _begin+_cen, _end+_cen)
    }
    HEADING = HEADING sprintf(" %d %g", _num, _ctime)

    if (_is_dscan) {        # dscan
        local i, s, cen

        s = "dscan_cleanup "
        for (i = 0; i < _nm; i++) {
            cen = A[_m[i]]
            s = s sprintf("%s %s ", motor_mne(_m[i]), cen)
            SCAN_POINTS[][i] += cen
        }
        s = s ";"
        cdef("cleanup_once", s, "dscan")
    }

    _stype = scanType_MotorScan
    # OLD _array_scan
    _raxr_scan

    if (_is_dscan) {        # dscan
        local s

        s = strdef("cleanup_once", "dscan")
        cdef("cleanup_once", "", "dscan", "delete")
        eval(s)
    }
}'


#%UI% 
#%MDESC% 
# this is _array_scan from 6.06.11 with as is
# to make this work with _raxr_scan_prep
def _raxr_scan '{
    local i, j, lo, hi, s, cols, mne

    cols = array_op("cols", SCAN_POINTS)
    _n1 = array_op("rows", SCAN_POINTS)

    if (_n1 < 1) {
        eprint "Intervals <= 0"
        exit
    }
    if (!(_stype&scanType_MeshScan))
        _n2 = 1
    else if (_n2 <= 1) {
        eprint "Mesh intervals <= 0"
        exit
    }

    FPRNT = VPRNT = ""
    _cols = 0

    if (_stype&scanType_EnergyScan)
        X_L = "Energy"
    if (_stype&(scanType_Parametric|scanType_EnergyScan)) {
        FPRNT = FPRNT sprintf("%s  ", X_L)
        VPRNT = VPRNT sprintf("%10s ", X_L)
        _cols++
    }
    if (_stype&scanType_Multi) {
        for (i = 0; i in X_LM; i++) {
            FPRNT = FPRNT sprintf("%s  ", X_LM[i])
            VPRNT = VPRNT sprintf("%10s ", X_LM[i])
            _cols++
        }
    }
    if (_stype&scanType_HKL_Scan) {
        # _hkl_col is usually 3, but is 2 for planar geometries
        for (i = 1; i <= _hkl_col; i++) {
            j = substr(_hkl_sym2, i, 1)
            FPRNT = FPRNT sprintf("%s  ", j)
            VPRNT = VPRNT sprintf("%10s ", j)
        }
        if (!(_stype&(scanType_Parametric|scanType_Multi)))
            X_L = substr(_hkl_sym2, _m1 - _nm + 1 , 1)
        _cols += _hkl_col
        if (_sav_geo_mot)
            _cols += _numgeo
    }

    # Find min and max for each column.  Limit checks on motors.
    _bad_lim = 0
    for (j = 0; j < cols; j++) {
        _s[j] = array_op("min", SCAN_POINTS[][j])
        _f[j] = array_op("max", SCAN_POINTS[][j])

        if (j >= _nm)
            continue

        mne = _m[j]
        lo = user(mne, get_lim(mne, -10))
        hi = user(mne, get_lim(mne, 10))
        if (lo > hi) {
            local tmp
            tmp = lo; lo = hi; hi = tmp
        }
        if (_s[j] < lo) {
            s = sprintf("low limit at %g", get_lim(mne, -1))
            _bad_lim = -1
        } else if (_f[j] > hi) {
            s = sprintf("high limit at %g", get_lim(mne, 1))
            _bad_lim = 1
        }
        if (_bad_lim) {
            eprintf("%s will hit %s (dial).\n", motor_name(mne), s)
            if (_stype&scanType_HKL_Scan) {
                s = ""
                for (i = 1; i <= _hkl_col; i++)
                    s = s substr(_hkl_sym2, i, 1) " "
                s = s "="
                for (i = 0; i < _hkl_col; i++)
                    s = s sprintf(" %g", Q[i])
                eprintf("(%s)\n", s)
            }
            exit
        }
        if (_stype&scanType_HKL_Scan) {
            if (_sav_geo_mot)
                FPRNT = FPRNT sprintf("%s  ", motor_name(mne))
        } else {
            FPRNT = FPRNT sprintf("%s  ", motor_name(mne))
            VPRNT = VPRNT sprintf("%9.9s ", motor_name(mne))
        }
    }
    _sx = _s[_m1]
    _fx = _f[_m1]
    if (_stype&scanType_EnergyScan) {
        _cols += _nm
    } else if (_stype&scanType_MotorScan) {
        X_L = motor_name(_m[0])
        _cols += _nm + _hkl_col
        for (i = 1; i <= _hkl_col; i++)
            FPRNT = FPRNT sprintf("%s  ", substr(_hkl_sym2, i, 1))
    }

    _n1 /= _n2
    Y_L = cnt_name(DET)
    
    # make sure we are in charge of the header
    if ( _header_for_raxr_ascan == 1 ) { 
        scan_head
    }


    _g1 = _g2 = _g3 = NPTS = 0

    def _raxr_scan_on \'
     for (; NPTS < _n1*_n2; _g1++, _g2++, NPTS++) {
        local i

        if (_g1 == int(_n1)) {       # only happens for mesh scan
            _g3 += _g1
            _g1 = 0
            if (_g2)
                print
        }
        for (i = 0; i < _nm;i++)
            A[_m[i]] = SCAN_POINTS[NPTS][i]

        scan_move

        FPRNT = VPRNT = ""
        if (_stype&(scanType_Parametric|scanType_EnergyScan)) {
            FPRNT = FPRNT sprintf("%g ", SCAN_POINTS[NPTS][_m1])
            VPRNT = VPRNT sprintf("%10.5g ", SCAN_POINTS[NPTS][_m1])
        }
        if (_stype&scanType_Multi) {
            for (i = 0; i in X_LM; i++) {
                local j

                j = _nm
                if (_stype&scanType_HKL_Scan)
                    j += _hkl_col
                FPRNT = FPRNT sprintf("%g ", SCAN_POINTS[NPTS][j+i])
                VPRNT = VPRNT sprintf("%10.5g ", SCAN_POINTS[NPTS][j+i])
            }
        }
        if (_stype&scanType_HKL_Scan) {
            for (i = 0; i < _hkl_col; i++) {
                FPRNT = FPRNT sprintf("%g ", SCAN_POINTS[NPTS][_nm + i])
                VPRNT = VPRNT sprintf("%10.5g ", SCAN_POINTS[NPTS][_nm + i])
            }
            if (_sav_geo_mot) {
                for (i = 0; i < _nm; i++)
                     FPRNT = FPRNT sprintf("%.8g ", A[_m[i]])
            }
        } else if (_stype&scanType_MotorScan) {
            for (i = 0; i < _nm; i++) {
                 FPRNT = FPRNT sprintf("%.8g ", A[_m[i]])
                 VPRNT = VPRNT sprintf("%9.*f ", UP, A[_m[i]])
            }
            if (!(_stype&scanType_EnergyScan))
                for (i = 0; i < _hkl_col; i++)
                    FPRNT = FPRNT sprintf("%g ", Q[i])
        }
        if (_stype&scanType_ArrayTimes )
            _ctime = SCAN_TIMES[NPTS]
        scan_loop
        scan_data(_g1, SCAN_POINTS[NPTS][_m1])
        scan_plot
     }
     scan_tail
    \'

    _raxr_scan_on
}'


#%MACROS%
#%IMACROS%
#%AUTHOR%
# Staffan Ohlsson, May 2017
#%TOC%
